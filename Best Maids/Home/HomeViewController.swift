//
//  HomeViewController.swift
//  Best Maids
//
//  Created by Mohammed Salah on 8/5/18.
//  Copyright © 2018 Mohammed Salah. All rights reserved.
//

import UIKit
import SideMenuSwift

enum PickerType {
    case Date
    case StartTime
    case EndTime
}

class HomeViewController: UIViewController {

    var pickerType = PickerType.Date
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblEndTime: UILabel!
    @IBOutlet weak var lblStartTime: UILabel!
    @IBOutlet weak var picker: UIPickerView!
    @IBOutlet var toTapGesture: UITapGestureRecognizer!
    @IBOutlet var fromTapGesture: UITapGestureRecognizer!
    
    @IBOutlet var startDateTapGesture: UITapGestureRecognizer!
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        picker.dataSource = self
        
        datePicker.minimumDate = Date().tomorrow
        datePicker.addTarget(self, action: #selector(HomeViewController.datePickerValueChanged(_:)), for: .valueChanged)
        
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        lblDate.text = dateFormatter.string(from: Date().tomorrow)
        lblStartTime.text = "08:00"
        lblEndTime.text = "09:00"
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    @IBAction func didPressTo(_ sender: UITapGestureRecognizer) {
        picker.isHidden = false
        pickerType = .EndTime
    }
    
    @IBAction func didPressFrom(_ sender: UITapGestureRecognizer) {
        picker.isHidden = false
        pickerType = .StartTime
    }
    
    @IBAction func didPressStartDate(_ sender: UITapGestureRecognizer) {
        datePicker.isHidden = false
        pickerType = .Date
    }
    
    
    @IBAction func didPressCall(_ sender: UIButton) {
        
    }
    
    
    @objc func datePickerValueChanged(_ sender: UIDatePicker){
        
        // Create date formatter
        let dateFormatter: DateFormatter = DateFormatter()
        
        // Set date format
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        // Apply date format
        let selectedDate: String = dateFormatter.string(from: sender.date)
        lblDate.text = selectedDate
        datePicker.isHidden = true
        print("Selected value \(selectedDate)")
    }
    
  
    
    
}

extension UIViewController {
    
    @IBAction func menuButtonDidClicked(_ sender: Any) {
        self.sm_sideMenuController?.revealMenu()
    }
}

extension HomeViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return 24
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return "\(row):00"
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerType == .StartTime {
            lblStartTime.text = "\(row):00"
        } else if pickerType == .EndTime {
            lblEndTime.text = "\(row):00"
        }
        picker.isHidden = true
    }
   
}
