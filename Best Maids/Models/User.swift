//
//  User.swift
//  Best Maids
//
//  Created by Mohammed Salah on 8/4/18.
//  Copyright © 2018 Mohammed Salah. All rights reserved.
//

import UIKit
import RealmSwift

class User: Object {
    
   @objc dynamic var userName = ""
   @objc dynamic var name = ""
   @objc dynamic var email = ""
   @objc dynamic var address = ""
   @objc dynamic var phoneNumber = ""
    @objc dynamic var userID = 0
   @objc dynamic var status = false
    

    override static func primaryKey() -> String? {
        return "userID"
    }
    
}

