//
//  ListOfImages.swift
//  Best Maids
//
//  Created by Mohammed Salah on 8/12/18.
//  Copyright © 2018 Mohammed Salah. All rights reserved.
//

import UIKit

struct ListOfImages: Decodable {

    struct Image: Decodable {
        
        var gallery_name = ""
        var gallery_image = ""
        var gallery_description = ""
    }
    
    var images = [Image]()
    
    enum CodingKeys: String, CodingKey {
            case images = "data"
    }
    
    
}
