//
//  Constants.swift
//  Best Maids
//
//  Created by Mohammed Salah on 8/4/18.
//  Copyright © 2018 Mohammed Salah. All rights reserved.
//

import UIKit

class Constants: NSObject {

    static var _FOLDER = "http://bestmaidsbuildingcleaning.com/bestapp/public/";
    static var _API_FOLDER = "http://bestmaidsbuildingcleaning.com/bestapp/public/api/"
    static var PATH_TO_SERVER_LOGIN:String = "\(_API_FOLDER)login";
    static var PATH_TO_SERVER_SIGN_UP:String = "\(_API_FOLDER)signin";
    static var PATH_TO_STORE:String = "\(_API_FOLDER)store";
    static var PATH_TO_GALLERY:String = "http://bestmaidsbuildingcleaning.com/admin_ali/api/get-gallery.php?latest_gallery";
    static var PATH_TO_THUMBS = "http://bestmaidsbuildingcleaning.com/admin_ali/upload/gallery/thumbs/"
    static var PATH_TO_IMAGE = "http://bestmaidsbuildingcleaning.com/admin_ali/upload/gallery/"
    
    // PARAMS
     static var ID = "id";
     static var HOURPRICE = "hourprice";
     static var FULLNAME = "fullname";
     static var USERNAME = "username";
     static var EMAIL = "email";
     static var PASSWORD = "password";
     static var ADDRESS = "address";
     static var PHONE = "phone";
     static var USER_ID = "user_id";
     static var PRODUCT_ID = "PRODUCT_ID";
     static var PRODUCT_NAME = "PRODUCT_NAME";
     static var FAVORITE_ID = "FAVORITE_ID";
    
     static var TITLE_MESSAGE = "title";
     static var MESSAGE_BODY = "message";
     static var USER_MESSAGE = "name";
     static var EMAIL_MESSAGE = "email";
     static var PHONE_MESSAGE = "phonenumber";

    
}

enum Navigator:String {
    case About = "about"
    case Social = "social"
    case Gallery = "gallery"
    case Profile = "profile"
    case Logout = "logout"
    case Share = "share"
}

extension Date {
    var yesterday: Date {
        return Calendar.current.date(byAdding: .day, value: -1, to: noon)!
    }
    var tomorrow: Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: noon)!
    }
    var noon: Date {
        return Calendar.current.date(bySettingHour: 12, minute: 0, second: 0, of: self)!
    }
    var month: Int {
        return Calendar.current.component(.month,  from: self)
    }
    var isLastDayOfMonth: Bool {
        return tomorrow.month != month
    }
}
