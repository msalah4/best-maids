//
//  StoreObject.swift
//  Best Maids
//
//  Created by Mohammed Salah on 8/11/18.
//  Copyright © 2018 Mohammed Salah. All rights reserved.
//

import UIKit

struct StoreObject: Decodable {

    var id:Int = 0
    
    var name = "" 
    var desc = ""
    var address = ""
    var phone = ""
    var website = ""
    var email = ""
    var hourrate = ""
    var discount = ""
    var opening_time = ""
 
    enum CodingKeys: String, CodingKey {
        case name
        case desc = "description"
        case address
        case phone
        case website
        case email
        case hourrate
        case discount
        case opening_time
    }
    
}
