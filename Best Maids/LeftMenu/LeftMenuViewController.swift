//
//  LeftMenuViewController.swift
//  Best Maids
//
//  Created by Mohammed Salah on 8/5/18.
//  Copyright © 2018 Mohammed Salah. All rights reserved.
//

import UIKit
import RealmSwift
import SideMenuSwift


class LeftMenuViewController: UIViewController {

    let images = [#imageLiteral(resourceName: "information"),#imageLiteral(resourceName: "ic_drawer_social"), #imageLiteral(resourceName: "image-gallery"), #imageLiteral(resourceName: "ic_profile"), #imageLiteral(resourceName: "share"), #imageLiteral(resourceName: "logout1")]
    let texts = ["About Us", "Social", "Gallery", "Profile", "Share", "Logout"]
    
    @IBOutlet weak var fullNAme: UILabel!
    @IBOutlet weak var email: UILabel!
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let user = realm.objects(User.self).first as! User
        
        fullNAme.text = user.name
        email.text = user.email
        SideMenuController.preferences.basic.defaultCacheKey = "default"
        
        sm_sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: Navigator.About.rawValue) }, with: "0")
        sm_sideMenuController?.cache(viewControllerGenerator: { self.storyboard?.instantiateViewController(withIdentifier: Navigator.Profile.rawValue) }, with: "1")

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func navigateForIndex(index:Int) {
       
        var id = ""
        
        if index == 0 {
            id = Navigator.About.rawValue
        } else if index == 1 {
            id = Navigator.Social.rawValue
        } else if index == 2 {
            id = Navigator.Gallery.rawValue
        } else if index == 3 {
            id = Navigator.Profile.rawValue
        } else if index == 4 {
            id = Navigator.Share.rawValue
        } else if index == 5 {
            id = Navigator.Logout.rawValue
        }
        
        let nav = sm_sideMenuController?.contentViewController as! UINavigationController
        let vController = self.storyboard?.instantiateViewController(withIdentifier: id)
        nav.show(vController!, sender: nil)
        self.sm_sideMenuController?.hideMenu()
//        self.sm_sideMenuController?.setContentViewController(with: "\(index)")
//        sm_sideMenuController?.hideMenu()
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LeftMenuViewController: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let row = indexPath.row
        navigateForIndex(index: row)
    }
}

extension LeftMenuViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        if section == 0 {
            return 6
//        } else {
//            return 1
//        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell?
        
        cell = tableView.dequeueReusableCell(withIdentifier: "CELL")
        
        if cell == nil  {
            cell = UITableViewCell()
        }
        let index = indexPath.section == 0 ? indexPath.row:5
        cell?.textLabel?.text = texts[index]
        cell?.imageView?.image = images[index]
        
        return cell!
    }
    
    
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 2
//    }
    
//    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
//        return ["", "Communicate"]
//    }
    
    
    
}
