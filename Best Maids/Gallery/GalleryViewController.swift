//
//  ViewController.swift
//  INSPhotoGallery
//
//  Created by Michal Zaborowski on 04.04.2016.
//  Copyright © 2016 Inspace Labs Sp z o. o. Spółka Komandytowa. All rights reserved.
//

import UIKit
import INSPhotoGallery
import Alamofire

class GalleryViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var useCustomOverlay = false
    var listOfPhotos = [INSPhotoViewable] ()
//    lazy var photos: [INSPhotoViewable] = {
//        return [
//            INSPhoto(imageURL: URL(string: "http://inspace.io/assets/portfolio/thumb/13-3f15416ddd11d38619289335fafd498d.jpg"), thumbnailImage: UIImage(named: "thumbnailImage")!),
//            INSPhoto(imageURL: URL(string: "http://inspace.io/assets/portfolio/thumb/13-3f15416ddd11d38619289335fafd498d.jpg"), thumbnailImage: UIImage(named: "thumbnailImage")!),
//            INSPhoto(image: UIImage(named: "fullSizeImage")!, thumbnailImage: UIImage(named: "thumbnailImage")!),
//            INSPhoto(imageURL: URL(string: "http://inspace.io/assets/portfolio/thumb/6-d793b947f57cc3df688eeb1d36b04ddb.jpg"), thumbnailImageURL: URL(string: "http://inspace.io/assets/portfolio/thumb/6-d793b947f57cc3df688eeb1d36b04ddb.jpg")),
//            INSPhoto(imageURL: URL(string: "http://inspace.io/assets/portfolio/thumb/6-d793b947f57cc3df688eeb1d36b04ddb.jpg"), thumbnailImageURL: URL(string: "http://inspace.io/assets/portfolio/thumb/6-d793b947f57cc3df688eeb1d36b04ddb.jpg")),
//            INSPhoto(imageURL: URL(string: "http://inspace.io/assets/portfolio/thumb/6-d793b947f57cc3df688eeb1d36b04ddb.jpg"), thumbnailImageURL: URL(string: "http://inspace.io/assets/portfolio/thumb/6-d793b947f57cc3df688eeb1d36b04ddb.jpg"))
//
//        ]
//    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.delegate = self
        collectionView.dataSource = self
//        for photo in photos {
//            if let photo = photo as? INSPhoto {
//                #if swift(>=4.0)
//                    photo.attributedTitle = NSAttributedString(string: "Example caption text\ncaption text", attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
//                #else
//                    photo.attributedTitle = NSAttributedString(string: "Example caption text\ncaption text", attributes: [NSForegroundColorAttributeName: UIColor.white])
//                #endif
//            }
//        }
        loadData()
    }
    
    
    func loadData () {
        Alamofire.request(Constants.PATH_TO_GALLERY,method:.get).responseJSON { (response) in
            
            if !response.result.isSuccess {
                //Error
            }
            if let jsonData = response.data
            {
                let decoder = JSONDecoder()
                let listOfImages = try! decoder.decode(ListOfImages.self, from: jsonData)
                
                for image in listOfImages.images {
                    let photo = INSPhoto(imageURL: URL(string: "\(Constants.PATH_TO_THUMBS)\(image.gallery_image)"), thumbnailImageURL: URL(string: "\(Constants.PATH_TO_IMAGE)\(image.gallery_image)"))
                    photo.attributedTitle = NSAttributedString(string: image.gallery_description, attributes: [NSAttributedStringKey.foregroundColor: UIColor.white])
                    self.listOfPhotos.append(photo)
                    self.collectionView.reloadData()
                }
              
                
            }
            
            
            
        }
    }
}

extension GalleryViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCollectionViewCell", for: indexPath) as! GalleryCollectionViewCell
        cell.populateWithPhoto(listOfPhotos[(indexPath as NSIndexPath).row])
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listOfPhotos.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! GalleryCollectionViewCell
        let currentPhoto = listOfPhotos[(indexPath as NSIndexPath).row]
        let galleryPreview = INSPhotosViewController(photos: listOfPhotos, initialPhoto: currentPhoto, referenceView: cell)
//        if useCustomOverlay {
//            galleryPreview.overlayView = CustomOverlayView(frame: CGRect.zero)
//        }
        
        galleryPreview.referenceViewForPhotoWhenDismissingHandler = { [weak self] photo in
            if let index = self?.listOfPhotos.index(where: {$0 === photo}) {
                let indexPath = IndexPath(item: index, section: 0)
                return collectionView.cellForItem(at: indexPath) as? GalleryCollectionViewCell
            }
            return nil
        }
        present(galleryPreview, animated: true, completion: nil)
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize.init(width: ((collectionView.frame.size.width / 2) - 20), height: 145)
    }
}
