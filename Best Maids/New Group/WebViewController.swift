//
//  ViewController.swift
//
//  Created by Mohammed Salah on 1/1/17.
//  Copyright © 2017 Mohammed Salah. All rights reserved.
//

import UIKit
import XLPagerTabStrip

class WebViewController: UIViewController ,UIWebViewDelegate, IndicatorInfoProvider{

    @IBOutlet weak var webview: UIWebView!
    var URL_FIRST_PAGE = "http://b.alannsar.aios-system.com/"
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!

    var itemInfo = IndicatorInfo(title: "View")

    
    override func viewDidLoad() {
        super.viewDidLoad()
        webview.delegate = self
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        reloadWebViewWithUrl(newUrl: URL_FIRST_PAGE);
        
    }
    
    // MARK: -webview
    
    func reload() {
        reloadWebViewWithUrl(newUrl: URL_FIRST_PAGE);
    }
    
    func reloadWebViewWithUrl(newUrl:String)  {
        
        let webUrl = URL(string: newUrl)
        let request = URLRequest (url: webUrl!)
        webview.loadRequest(request)
    }

    
    func webViewDidStartLoad(_ webView: UIWebView) {
        activityIndicatorView.isHidden = false;
        activityIndicatorView.startAnimating()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        activityIndicatorView.stopAnimating()
        activityIndicatorView.isHidden = true;
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error){
        activityIndicatorView.stopAnimating()
        activityIndicatorView.isHidden = true;
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func indicatorInfo(for pagerTabStripController: PagerTabStripViewController) -> IndicatorInfo {
        return itemInfo
    }

}

