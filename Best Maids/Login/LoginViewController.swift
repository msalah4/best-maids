//
//  LoginViewController.swift
//  Best Maids
//
//  Created by Mohammed Salah on 8/4/18.
//  Copyright © 2018 Mohammed Salah. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class LoginViewController: UIViewController {

    let realm = try! Realm()
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func didLoginPressed(_ sender: UIButton) {
        
        if (emailTF.text?.isEmpty)! || (passwordTF.text?.isEmpty)! {
            return
        }
        
        let params = [Constants.EMAIL:emailTF.text!, Constants.PASSWORD:passwordTF.text!]
        Alamofire.request(Constants.PATH_TO_SERVER_LOGIN,method:.post ,parameters: params).responseJSON { (response) in
            
            if !response.result.isSuccess {
                //Error
            }

            if let json = (response.result.value!) as? Dictionary<String, Any> {
                let user = User()
                user.name = json[Constants.FULLNAME] as! String
                user.email = json[Constants.EMAIL] as! String
                user.address = json[Constants.ADDRESS] as! String
                user.phoneNumber = json[Constants.PHONE] as! String
                user.userName = json[Constants.USERNAME] as! String
                user.userID = json[Constants.ID] as! Int
                
                try! self.realm.write {
                    self.realm.add(user, update: true)
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
