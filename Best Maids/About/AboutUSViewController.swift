//
//  AboutUSViewController.swift
//  Best Maids
//
//  Created by Mohammed Salah on 8/11/18.
//  Copyright © 2018 Mohammed Salah. All rights reserved.
//

import UIKit
import Alamofire
class AboutUSViewController: UIViewController {

    
    
    @IBOutlet weak var aboutTitle: UILabel!
    @IBOutlet weak var lblPlaces: UILabel!
    @IBOutlet weak var btnPhoneNumbers: UIButton!
    @IBOutlet weak var btnSite: UIButton!
    @IBOutlet weak var btnEmail: UIButton!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblOpenTimes: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadData()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData() {
     
        Alamofire.request(Constants.PATH_TO_STORE,method:.get).responseJSON { (response) in
            
            if !response.result.isSuccess {
                //Error
            }
            if let jsonData = response.data
            {
                let decoder = JSONDecoder()
                let storeObject = try! decoder.decode(StoreObject.self, from: jsonData)
                
                DispatchQueue.main.async {
                    self.aboutTitle.text = storeObject.name
                    self.lblDescription.text = storeObject.desc
                    self.lblPlaces.text = storeObject.address
                    self.btnPhoneNumbers.setTitle(storeObject.phone, for: .normal)
                    self.btnSite.setTitle(storeObject.website, for: .normal)
                    self.btnEmail.setTitle(storeObject.email, for: .normal)
                }
                
            }
            
            
            
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
